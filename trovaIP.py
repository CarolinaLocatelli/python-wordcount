import re, socket

def findIP():
	file = open("testo.txt", "r", encoding="utf-8-sig") #apro il file in lettura
	pattern = re.compile(r"(\d{1,3}\.){3}\d{1,3}")
	wordcount={}
	for parola in file.read().lower().split():
		result = pattern.match(parola)
		if result:
			"""splittare parola usando il punto e verificare che substring sia tra 0 e 255"""
			valido=True
			for substring in parola.split('.'):
				#print(substring)
				if int(substring)<0 or int(substring)>255:
					valido=False
					
			if valido==True:
				print(parola)
	file.close()
	
def findURL():
	file = open("testo.txt", "r", encoding="utf-8-sig") #apro il file in lettura
	pattern = re.compile(r"https?:\/\/(\w+(?:\.\w+)*)((?:\/\w+)*)")
	wordcount={}
	for parola in file.read().lower().split():
		result = pattern.match(parola)
		if result:
			hostname = result.group(1)
			
			try:
				print(hostname + ": " + socket.gethostbyname(hostname))
			except:
				print(hostname + ": " + 'Hostname non valido')
	file.close()

findIP()
findURL()
