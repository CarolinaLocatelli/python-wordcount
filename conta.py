#from operator import itemgetter
import re

def wordcount():
	"""funzione che conta le parole in un file di testo e stampa la relativa frequenza"""
	file = open("testo.txt", "r", encoding="utf-8-sig") #apro il file in lettura
	wordcount = {} #istanzio il dizionario che conterrà parole e frequenze

	#leggo ogni parola eliminando punteggiatura e popolo il dizionario con chiave=parola e valore=contatore
	#for parola in file.read().replace(',','').replace(';','').replace('.','').replace(':','').lower().split():
	pattern = re.compile(r"\w+")
	for parola in file.read().lower().split():
		result = pattern.match(parola)
		if result:
			if parola in wordcount:
				wordcount[parola]+=1
			else:
				wordcount[parola]=1
	#stampo chiavi e valori		
	#for chiave,valore in wordcount.items():
		#print(chiave,valore)
		
	#sort 
	#wordcount_sorted=sorted(wordcount.items(), key=itemgetter(1), reverse=True)
	wordcount_sorted=sorted(wordcount.items(), key=lambda x:x[1], reverse=True)
	
                        
	print("___PAROLE ORDINATE___")
	for item in wordcount_sorted:
		print(item)
	file.close()
	
	
def lettercount():
	file = open("testo.txt", "r", encoding="utf-8-sig") #apro il file in lettura
	lettercount = {} #istanzio il dizionario che conterrà lettere e frequenze
	
	#tratto l'input come un'unica stringa
	pattern = re.compile(r"\w")
	#for lettera in file.read().replace(',','').replace(';','').replace('.','').replace(' ','').replace(':','').lower():
	for lettera in file.read().lower():
		result = pattern.match(lettera)
		if result:
			if lettera in lettercount:
				lettercount[lettera]+=1
			else:
				lettercount[lettera]=1

	#lettercount_sorted = sorted(lettercount.items(), key=itemgetter(1), reverse=True)
	lettercount_sorted=sorted(lettercount.items(), key=lambda x:x[1], reverse=True)
	print("\n___LETTERE ORDINATE___")
	for item in lettercount_sorted:
		print(item)
	file.close()
	
		
wordcount()
lettercount()
